Privacy Policy
==============

Last Updated: 12.07.2022

This Privacy Policy describes the policies and procedures of GNUnet e.V.
(“we,” “our,” or “us”) pertaining to the collection, use, and
disclosure of your information on our sites and related mobile
applications and products we offer (the “Services”). This Privacy
Statement applies to your personal data when you use our Services, and
does not apply to online websites or services that we do not own or
control.


Overview
--------

Your privacy is important to us. We follow a few fundamental
principles: We don’t ask you for personally identifiable information
(defined below). That being said, your contact information, such as
your phone number, social media handle, or email address (depending on
how you contact us), may be collected when you communicate with us,
for example to report a bug or other error related to TalDir. We
don’t share your information with third parties except when strictly
required to deliver you our Services and products, or to comply with
the law. If you have any questions or concerns about this policy,
please reach out to us at taldir@gnunet.org.


How you accept this policy
--------------------------

By using our Services or visiting our sites, you agree to the use, disclosure,
and procedures outlined in this Privacy Policy.


What personal information do we collect from our users?
-------------------------------------------------------

The information we collect from you falls into two categories: (i) personally
identifiable information (i.e., data that could potentially identify you as an
individual) (“Personal Information”), and (ii) non-personally identifiable
information (i.e., information that cannot be used to identify who you are)
(“Non-Personal Information”). This Privacy Policy covers both categories and
will tell you how we might collect and use each type.

We do our best to not collect any Personal Information from TalDir
users.  The detailed Personal Information TalDir asks from you
during the regular registration process at the beginning is
processed to verify that your are actually the holder of a certain account.
The information is never stored in plain text and only used to create a
cryptographic account identifier which does not allow us to recover any of your
details.

That being said, when using our Services to register a mapping from your
identity to a wallet, we may inherently receive the following information
(depending on your choice of authentication method):

   * Your phone number when using SMS authentication. We rely on third party providers (such as your mobile network operator) to deliver the SMS to you. These third parties will see the SMS message sent to you and could thus learn that you are using TalDir. SMS is inherently insecure, and you should expect many governments and private parties to be able to observe these messages.  However, we do not store your phone number for SMS communication on our systems, except maybe in short-term logs to diagnose errors.

   * Your e-mail address when using E-mail authentication. We rely on the Internet and your E-mail provider to deliver the E-mail to you. Internet service providers will see the E-mail message sent to you and could thus learn that you are using TalDir. E-mail is inherently insecure, and you should expect many governments and private parties to be able to observe these messages.  However, we do not store your E-mail address on our systems, except maybe in short-term logs to diagnose errors.

   * Your twitter handle when using Twitter authentication. We rely on Twitter to deliver a message to you. Twitter will see the message sent to you and could thus learn that you are using TalDir. Twitter is inherently insecure, and you should expect many governments and private parties to be able to observe these messages.  However, we do not store your twitter handle on our systems, except maybe in short-term logs to diagnose errors.

   * When you contact us. We may collect certain information if you choose to contact us, for example to report a bug or other error with the Taler Wallet. This may include contact information such as your name, email address or phone number depending on the method you choose to contact us. We strictly only use the information provided by you in these instances to answer your request or to deliver the services requested by you.


How we collect and process personal data
----------------------------------------

We may process your personal data for the following reasons:

   * to authenticate you during registration
   * to support you using Taldirs when you contact us


How we share and use the information we gather
----------------------------------------------

We may share your authentication data with other providers that assist
us in performing the authentication. We will try to use providers that
to the best of our knowledge respect your privacy and have good
privacy practices.  We reserve the right to change authentication
providers at any time to ensure availability of our services.

We primarily use the limited information we receive directly from you to
enhance TalDir. Some ways we may use your Personal Information are
to: Contact you when necessary to respond to your comments, answer your
questions, or obtain additional information on issues related to bugs or
errors with the TalDir application that you reported.


Agents or third party partners
------------------------------

We may provide your Personal Information to our employees, contractors,
agents, service providers, and designees (“Agents”) to enable them to perform
certain services for us exclusively, including: improvement and maintenance of
our software and Services.


Protection of us and others
---------------------------

We reserve the right to access, read, preserve, and disclose any information
that we reasonably believe is necessary to comply with the law or a court
order.


What personal information can I access or change?
-------------------------------------------------

You can request access to the information we have collected from
you. You can do this by contacting us at taldir@gnunet.org. We will
make sure to provide you with a copy of the data we process about
you. To comply with your request, we may ask you to verify your
identity. We will fulfill your request by sending your copy
electronically. For any subsequent access request, we may charge you
with an administrative fee. If you believe that the information we
have collected is incorrect, you are welcome to contact us so we can
update it and keep your data accurate. Any data that is no longer
needed for purposes specified in the “How We Use the Information We
Gather” section will be deleted after ninety (90) days.


What are your data protection rights?
-------------------------------------

GNUnet e.V. would like to make sure you are fully aware of all of your
data protection rights. Every user is entitled to the following:

**The right to access**: You have the right to request GNUnet e.V. for
 copies of your personal data. We may charge you a small fee for this
 service.

**The right to rectification**: You have the right to request that
GNUnet e.V. correct any information you believe is inaccurate. You also
have the right to request GNUnet e.V. to complete information you
believe is incomplete.  The right to erasure - You have the right to
request that GNUnet e.V. erase your personal data, under certain
conditions.

**The right to restrict processing**: You have the right to request
 that GNUnet e.V. restrict the processing of your personal data, under
 certain conditions.

**The right to object to processing**: You have the right to object to
 GNUnet e.V.'s processing of your personal data, under certain
 conditions.

**The right to data portability**: You have the right to request that
 GNUnet e.V. transfer the data that we have collected to another
 organization, or directly to you, under certain conditions.

If you make a request, we have one month to respond to you. If you
would like to exercise any of these rights, please contact us at our
email: taldir@gnunet.org

You can always contact your local data protection authority to enforce
your rights.


Data retention
--------------

Information entered into our bug tracker will be retained indefinitely
and is typically made public. We will only use it to triage the
problem.  Beyond that, we do not retain personally identifiable
information about our users for longer than one week.


Data security
-------------

We are committed to making sure your information is protected. We employ
several physical and electronic safeguards to keep your information safe,
including encrypted user passwords, two factor verification and authentication
on passwords where possible, and securing connections with industry standard
transport layer security. You are also welcome to contact us using GnuPG
encrypted e-mail. Even with all these precautions, we cannot fully guarantee
against the access, disclosure, alteration, or deletion of data through
events, including but not limited to hardware or software failure or
unauthorized use. Any information that you provide to us is done so entirely
at your own risk.


Changes and updates to privacy policy
-------------------------------------

We reserve the right to update and revise this privacy policy at any time. We
occasionally review this Privacy Policy to make sure it complies with
applicable laws and conforms to changes in our business. We may need to update
this Privacy Policy, and we reserve the right to do so at any time. If we do
revise this Privacy Policy, we will update the “Effective Date” at the top
of this page so that you can tell if it has changed since your last visit. As
we generally do not collect contact information and also do not track your
visits, we will not be able to notify you directly. However, TalDir clients
may inform you about a change in the privacy policy once they detect that the
policy has changed. Please review this Privacy Policy regularly to ensure that
you are aware of its terms. Any use of our Services after an amendment to our
Privacy Policy constitutes your acceptance to the revised or amended
agreement.


International users and visitors
--------------------------------

Our Services are (currently) hosted in Germany. If you are a user
accessing the Services from Switzerland, Asia, US, or any other
region with laws or regulations governing personal data collection,
use, and disclosure that differ from the laws of Germany, please be
advised that through your continued use of the Services, which is
governed by the law of the country hosting the service, you are
transferring your Personal Information to Germany and you consent to
that transfer.


Questions
---------

Please contact us at taldir@gnunet.org if you have questions about our
privacy practices that are not addressed in this Privacy Statement.
