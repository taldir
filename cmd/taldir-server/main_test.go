// This file is part of taldir, the Taler Directory implementation.
// Copyright (C) 2022 Martin Schanzenbach
//
// Taldir is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// Taldir is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package main_test

import (
	"bytes"
	"crypto/sha512"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	gnunetutil "gnunet/util"

	"github.com/jarcoal/httpmock"
	_ "taler.net/taldir/cmd/taldir-server"
	"taler.net/taldir/internal/util"
	taldir "taler.net/taldir/pkg/rest"
)

var t taldir.Taldir

// Note: This duration will be rounded down to 20 Months (= 51840000000000)
var validRegisterRequest = []byte(`
  {
    "address": "abc@test",
    "public_key": "000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946A90",
    "inbox_url": "myinbox@xyz",
    "duration": 53135000000000
  }
`)

var validRegisterRequestUnmodified = []byte(`
  {
    "address": "abc@test",
    "public_key": "000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946A90",
    "inbox_url": "myinbox@xyz",
    "duration": 0
  }
`)

var newOrderMockResponse = `
  {
    "order_id": "testOrder1234",
    "taler_pay_uri": "payto://ladida"
  }
`

var newOrderStatusUnpaidMockResponse = `
  {
    "order_status": "unpaid",
    "taler_pay_uri": "payto://somedude"
  }
`

func TestMain(m *testing.M) {
	t.Initialize("testdata/taldir-test.conf")
	code := m.Run()
	t.ClearDatabase()
	os.Exit(code)
}

func getHAddress(addr string) string {
	h := sha512.New()
	h.Write([]byte(addr))
	return gnunetutil.EncodeBinaryToString(h.Sum(nil))
}

func TestNoEntry(s *testing.T) {
	t.ClearDatabase()

	h_addr := getHAddress("jdoe@example.com")
	req, _ := http.NewRequest("GET", "/"+h_addr, nil)
	response := executeRequest(req)

	if http.StatusNotFound != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusNotFound, response.Code)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	t.Router.ServeHTTP(rr, req)
	return rr
}

func TestRegisterRequest(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	file, err := os.Open("validation_code")
	if err != nil {
		s.Errorf("No validation code file found!\n")
	}
	code, err := ioutil.ReadAll(file)
	if err != nil {
		s.Errorf("Error reading validation code file contents!\n")
	}
	h_addr := getHAddress("abc@test")
	trimCode := strings.Trim(string(code), " \r\n")
	solution := util.GenerateSolution("000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946A90", trimCode)
	solutionJSON := "{\"solution\": \"" + solution + "\"}"
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusNoContent != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusNoContent, response.Code)
	}
}

func TestRegisterQRPageRequest(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	req, _ = http.NewRequest("GET", "/register/NonSenseAddr/NonSenseCode", nil)
	response = executeRequest(req)
	if http.StatusNotFound != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusNotFound, response.Code)
	}

	file, err := os.Open("validation_code")
	if err != nil {
		s.Errorf("No validation code file found!\n")
	}
	code, err := ioutil.ReadAll(file)
	if err != nil {
		s.Errorf("Error reading validation code file contents!\n")
	}
	h_addr := getHAddress("abc@test")
	trimCode := strings.Trim(string(code), " \r\n")
	req, _ = http.NewRequest("GET", "/register/"+h_addr+"/"+trimCode, nil)
	response = executeRequest(req)
	if http.StatusOK != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusOK, response.Code)
	}
}

func TestReRegisterRequest(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	file, err := os.Open("validation_code")
	if err != nil {
		s.Errorf("No validation code file found!\n")
	}
	code, err := ioutil.ReadAll(file)
	if err != nil {
		s.Errorf("Error reading validation code file contents!\n")
	}
	h_addr := getHAddress("abc@test")
	trimCode := strings.Trim(string(code), " \r\n")
	solution := util.GenerateSolution("000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946A90", trimCode)
	solutionJSON := "{\"solution\": \"" + solution + "\"}"
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusNoContent != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusNoContent, response.Code)
	}
	req, _ = http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequestUnmodified))
	response = executeRequest(req)

	if http.StatusOK != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusOK, response.Code)
	}

}

func TestReRegisterRequestTooMany(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	req, _ = http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response = executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	req, _ = http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response = executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	req, _ = http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response = executeRequest(req)

	if http.StatusTooManyRequests != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusTooManyRequests, response.Code)
	}

}

func TestSolutionRequestTooMany(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	h_addr := getHAddress("abc@test")
	solution := util.GenerateSolution("000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946A90", "wrongSolution")
	solutionJSON := "{\"solution\": \"" + solution + "\"}"
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusForbidden != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusForbidden, response.Code)
	}
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusForbidden != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusForbidden, response.Code)
	}
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusForbidden != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusForbidden, response.Code)
	}
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusTooManyRequests != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusTooManyRequests, response.Code)
	}

}

func TestRegisterRequestWrongPubkey(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/test", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusAccepted != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusAccepted, response.Code)
	}
	file, err := os.Open("validation_code")
	if err != nil {
		s.Errorf("No validation code file found!\n")
	}
	code, err := ioutil.ReadAll(file)
	if err != nil {
		s.Errorf("Error reading validation code file contents!\n")
	}
	h_addr := getHAddress("abc@test")
	trimCode := strings.Trim(string(code), " \r\n")
	solution := util.GenerateSolution("000G006XE97PTWV3B7AJNCRQZA6BF26HPV3XZ07293FMY7KD4181946AA0", trimCode)
	solutionJSON := "{\"solution\": \"" + solution + "\"}"
	req, _ = http.NewRequest("POST", "/"+h_addr, bytes.NewBuffer([]byte(solutionJSON)))
	response = executeRequest(req)
	if http.StatusForbidden != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusForbidden, response.Code)
	}
}

func TestUnsupportedMethod(s *testing.T) {
	t.ClearDatabase()

	req, _ := http.NewRequest("POST", "/register/email", bytes.NewBuffer(validRegisterRequest))
	response := executeRequest(req)

	if http.StatusNotFound != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusNotFound, response.Code)
	}
}

func TestPaymentRequiredMethod(s *testing.T) {
	t.ClearDatabase()
	t.MonthlyFee = "KUDOS:5"
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()
	req, _ := http.NewRequest("POST", "/register/test-cost", bytes.NewBuffer(validRegisterRequest))
	httpmock.RegisterResponder("POST", "http://merchant.taldir/instances/myInstance/private/orders", httpmock.NewStringResponder(200, newOrderMockResponse))
	httpmock.RegisterResponder("GET", "http://merchant.taldir/instances/myInstance/private/orders/testOrder1234", httpmock.NewStringResponder(200, newOrderStatusUnpaidMockResponse))

	response := executeRequest(req)
	t.MonthlyFee = "KUDOS:0"
	if http.StatusPaymentRequired != response.Code {
		s.Errorf("Expected response code %d. Got %d\n", http.StatusPaymentRequired, response.Code)
	}
}
