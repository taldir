module taler.net/taldir

go 1.18

require (
	github.com/gertd/go-pluralize v0.2.1
	github.com/gorilla/mux v1.8.0
	github.com/jarcoal/httpmock v1.2.0
	github.com/kataras/i18n v0.0.8
	github.com/schanzen/taler-go v1.0.6
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	gnunet v0.1.27
	gopkg.in/ini.v1 v1.67.0
	gorm.io/driver/postgres v1.3.4
	gorm.io/gorm v1.23.4
)

require (
	github.com/BurntSushi/toml v1.2.1 // indirect
	github.com/bfix/gospel v1.2.15 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.11.0 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.2.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20200714003250-2b9c44734f2b // indirect
	github.com/jackc/pgtype v1.10.0 // indirect
	github.com/jackc/pgx/v4 v4.15.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	golang.org/x/crypto v0.0.0-20220518034528-6f7dac969898 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace gnunet v0.1.27 => ./third_party/gnunet-go/src/gnunet
