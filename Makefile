all: gana server cli

SCRIPT_TARGET:=$(shell dirname $(shell go list -f '{{.Target}}' ./cmd/taldir-server))

gana:
	mkdir -p internal/gana
	git submodule update --init --recursive
	git submodule sync --recursive
	cd third_party/gana/gnu-taler-error-codes && make taler_error_codes.go
	cp third_party/gana/gnu-taler-error-codes/taler_error_codes.go internal/gana/

VERSION=`git describe --tags`

server:
	go build -ldflags "-X main.version=${VERSION}"  ./cmd/taldir-server

cli:
	go build -ldflags "-X main.version=${VERSION}" ./cmd/taldir-cli

install: server cli
	go install ./cmd/taldir-server && go install ./cmd/taldir-cli
	chmod +x scripts/validators/*
	cp scripts/validators/* $(SCRIPT_TARGET)

.PHONY: all gana
