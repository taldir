// This file is part of taldir, the Taler Directory implementation.
// Copyright (C) 2022 Martin Schanzenbach
//
// Taldir is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License,
// or (at your option) any later version.
//
// Taldir is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL3.0-or-later

package util

import (
	"crypto/sha512"
	"crypto/rand"
	gnunetutil "gnunet/util"
	"time"

	talerutil "github.com/schanzen/taler-go/pkg/util"
)

// Generates a solution from a challenge and pubkey
func GenerateSolution(targetUriEncoded string, challenge string) string {
	h := sha512.New()
	h.Write([]byte(challenge))
	h.Write([]byte(targetUriEncoded))
	return gnunetutil.EncodeBinaryToString(h.Sum(nil))
}

// Generates random reference token used in the validation flow.
func GenerateChallenge(bytes int) string {
	randBytes := make([]byte, bytes)
	_, err := rand.Read(randBytes)
	if err != nil {
		panic(err)
	}
	return gnunetutil.EncodeBinaryToString(randBytes)
}

// Check if this is a non-zero, positive amount
func CalculateCost(sliceCostAmount string, fixedCostAmount string, howLong time.Duration, sliceDuration time.Duration) (*talerutil.Amount, error) {
	sliceCount := int(float64(howLong.Microseconds()) / float64(sliceDuration.Microseconds()))
	sliceCost, err := talerutil.ParseAmount(sliceCostAmount)
	if nil != err {
		return nil, err
	}
	fixedCost, err := talerutil.ParseAmount(fixedCostAmount)
	if nil != err {
		return nil, err
	}
	sum := &talerutil.Amount{
		Currency: sliceCost.Currency,
		Value:    0,
		Fraction: 0,
	}
	for i := 0; i < sliceCount; i++ {
		sum, err = sum.Add(*sliceCost)
		if nil != err {
			return nil, err
		}
	}
	sum, err = sum.Add(*fixedCost)
	if nil != err {
		return nil, err
	}
	return sum, nil
}
